RxAP
===

![](logo.png)

---

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)

> 

# Projects

## Libraries

Name | License | Version | Dependencies | Description
--- | --- | --- | --- | ---
@rxap/nest | ![NPM](https://img.shields.io/npm/l/@rxap/nest?style=flat-square) | [![npm version](https://img.shields.io/npm/v/@rxap/nest?style=flat-square)](https://www.npmjs.com/package/@rxap/nest) | ![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/nest) | 
@rxap/nest-firebase | ![NPM](https://img.shields.io/npm/l/@rxap/nest-firebase?style=flat-square) | [![npm version](https://img.shields.io/npm/v/@rxap/nest-firebase?style=flat-square)](https://www.npmjs.com/package/@rxap/nest-firebase) | ![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/nest-firebase) | 
@rxap/nest-sentry | MIT | - | - | 
@rxap/nest-socket-io-client | MIT | - | - | 

