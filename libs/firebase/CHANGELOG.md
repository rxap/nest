# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [9.0.1](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@9.0.0...@rxap/nest-firebase@9.0.1) (2022-12-16)

**Note:** Version bump only for package @rxap/nest-firebase





# [9.0.0](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@8.0.0...@rxap/nest-firebase@9.0.0) (2022-10-07)


### chore

* update to nest 9.x.x ([1716aba](https://gitlab.com/rxap/nest/commit/1716aba489da188325bac795961b7cba2f4c8c35))


### BREAKING CHANGES

* update to nest 9.x.x





# [8.0.0](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@8.0.0-next.0...@rxap/nest-firebase@8.0.0) (2022-09-27)

**Note:** Version bump only for package @rxap/nest-firebase





# [8.0.0-next.0](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.3.0-next.2...@rxap/nest-firebase@8.0.0-next.0) (2022-02-19)


### Build System

* upgrade to nrwl 13.x.x ([6f8a5e8](https://gitlab.com/rxap/nest/commit/6f8a5e8f40f553fb5f07089c3f4153f00d600985))


### BREAKING CHANGES

* update the core nrwl packages to 13.x.x

Signed-off-by: Merzough Münker <mmuenker@digitaix.com>





# [7.3.0](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.3.0-next.2...@rxap/nest-firebase@7.3.0) (2022-02-19)

**Note:** Version bump only for package @rxap/nest-firebase





# [7.3.0-next.2](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.3.0-next.1...@rxap/nest-firebase@7.3.0-next.2) (2021-12-17)


### Bug Fixes

* handle firestore error with code 2 ([ecb0e0b](https://gitlab.com/rxap/nest/commit/ecb0e0b58c9bcf48a5bd38c017f01ef1cb78cd92))
* make all inject token optional ([4d7ded3](https://gitlab.com/rxap/nest/commit/4d7ded3fae1dffa3f062793432ef450c7a43023c))





# [7.3.0-next.1](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.3.0-next.0...@rxap/nest-firebase@7.3.0-next.1) (2021-11-26)


### Bug Fixes

* add forRoot option to deactivate app check and firebase auth ([15de3bf](https://gitlab.com/rxap/nest/commit/15de3bfc459eb5d0c73bfc039d63a0c3070cd375))





# [7.3.0-next.0](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.2.4-next.0...@rxap/nest-firebase@7.3.0-next.0) (2021-11-26)


### Features

* support app check and firebase auth guard deactivation ([fa3511b](https://gitlab.com/rxap/nest/commit/fa3511befb45906ecf04bf73a2574cff01771d25))





## [7.2.4-next.0](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.2.3...@rxap/nest-firebase@7.2.4-next.0) (2021-11-26)


### Bug Fixes

* add additionally logging ([1d709c3](https://gitlab.com/rxap/nest/commit/1d709c3df91d3633bb9bba031a9334989fe98eb0))





## [7.2.3](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.2.2...@rxap/nest-firebase@7.2.3) (2021-11-16)


### Bug Fixes

* add missing firebase module export ([d8893a7](https://gitlab.com/rxap/nest/commit/d8893a7862be356e2ffafff03cbdd33b575c461c))





## [7.2.2](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.2.1...@rxap/nest-firebase@7.2.2) (2021-11-16)


### Bug Fixes

* add missing firebase module export ([a9635c1](https://gitlab.com/rxap/nest/commit/a9635c174a1a69a45c4fc12a6c8f89f7ecb99ad7))





## [7.2.1](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.2.0...@rxap/nest-firebase@7.2.1) (2021-11-13)


### Bug Fixes

* **firestore-error-handler:** pass promise type ([7d3da12](https://gitlab.com/rxap/nest/commit/7d3da125d08ae97d4f0e7cb67c30da3bb629b8d6))





# [7.2.0](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.1.2...@rxap/nest-firebase@7.2.0) (2021-11-11)


### Features

* add a generic firestore error handler ([3f76fc1](https://gitlab.com/rxap/nest/commit/3f76fc1d8a1954ed58a5f91713540e172f8766fc))





## [7.1.2](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.1.1...@rxap/nest-firebase@7.1.2) (2021-11-11)


### Bug Fixes

* add missing type aliases ([64e800b](https://gitlab.com/rxap/nest/commit/64e800b137477673f1b2da38615ffe8a3957cf99))





## [7.1.1](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.1.0...@rxap/nest-firebase@7.1.1) (2021-11-10)


### Bug Fixes

* add AppCheckGuard to module providers ([47fb6fb](https://gitlab.com/rxap/nest/commit/47fb6fb1cb504c711ef93bd6cadb9bfe063d72d5))





# [7.1.0](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.0.3...@rxap/nest-firebase@7.1.0) (2021-11-10)


### Features

* add app check guard ([cc6a0e6](https://gitlab.com/rxap/nest/commit/cc6a0e6c17aba6cd441eacf695ce254abd222260))
* add basic documentation and add helper types and interfaces ([11a87a4](https://gitlab.com/rxap/nest/commit/11a87a4e19462553d9375d4dea3d42a65eb71c9e))
* add the firebase user decorator ([28b937c](https://gitlab.com/rxap/nest/commit/28b937ce63b28a9fb68cae0194cb9866604d2412))
* extend firebase auth support ([702e92b](https://gitlab.com/rxap/nest/commit/702e92b7fe83ce3401cef1d9c79b91a48633d8dc))





## [7.0.3](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.0.2...@rxap/nest-firebase@7.0.3) (2021-10-19)


### Bug Fixes

* **module:** set allowUnverifiedEmail default to false ([cc3edd5](https://gitlab.com/rxap/nest/commit/cc3edd54aaa622626016f7bd832c0dcb9d305c09))





## [7.0.2](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@7.0.1...@rxap/nest-firebase@7.0.2) (2021-07-06)


### Bug Fixes

* add missing dependencies ([7aeb92b](https://gitlab.com/rxap/nest/commit/7aeb92b1b80e2d4ef23b54cccd105d595fb49874))





## [7.0.1](https://gitlab.com/rxap/nest/compare/@rxap/nest-firebase@12.0.1...@rxap/nest-firebase@7.0.1) (2021-07-06)

**Note:** Version bump only for package @rxap/nest-firebase





## 12.0.1 (2021-07-06)

**Note:** Version bump only for package @rxap/nest-firebase
