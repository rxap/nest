Open the main `AppModule` file and add a new module import to the imports array.

```typescript
import { FirebaseModule } from '@rxap/nest-firebase';

@Module({
  imports: [
    FirebaseModule.forRoot()
  ]
})
export class AppModule {}
```
