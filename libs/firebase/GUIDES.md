## Initialisation

In general, the package is entirely compatible with the [official initialization process](https://firebase.google.com/docs/admin/setup#initialize-sdk).
If the nest application is deployed in a GCP-controlled environment (Cloud Function, AppEngine, ...)
the required configuration is provided with the environment variable `GOOGLE_APPLICATION_CREDENTIALS` or `FIREBASE_CONFIG`.

It is also possible to manually configure the initialization options.

The firebase initialization can be customized with `forRoot` method of the `FirebaseModule`.

The options object can be split into two parts:

### `initializeApp`

An object with the two properties `options` and `name` of the `admin.initializeApp`
method. And an alias `defaultProject` to set the `projectId` property of the `options` object.

### `auth`

An object with two properties `allowUnverifiedEmail` and `authHeaderName`.
If the property `allowUnverifiedEmail` is set to `true` the `FirebaseAuthGuard` does not check if the user email is verified. With the property `authHeaderName` the property key
used to access the firebase auth token is defined. The default value is `idToken`.

#### Full Example

```typescript
FirebaseModule.forRoot({
  initializeApp: {
    options: { ... },
    name: 'custom-app-name',
    defaultProject: 'overwrite-gcp-project-id'
  },
  auth: {
    allowUnverifiedEmail: true,
    authHeaderName: 'custom-auth-header'
  }
})
```

## Firebase Authentication

The integration of firebase authentication works out of the box.

### Guards

If each route should only be accessible for authenticated users, add the `APP_GUARD`
provider to the main `AppModule` providers array.

```typescript
import { FirebaseModule, FirebaseAuthGuard } from '@rxap/nest-firebase';
import { APP_GUARD } from '@nestjs/core';

@Module({
  imports: [
    FirebaseModule.forRoot()
  ],
  provides: [
    {
      provide:     APP_GUARD,
      useExisting: FirebaseAuthGuard
    }
  ]
})
export class AppModule {}
```

If only some controllers should be protected, add the `@UseGuards(FirebaseAuthGuard)`
decorator to the controller class.

```typescript
import { FirebaseAuthGuard } from '@rxap/nest-firebase';
import { UseGuards } from '@nestjs/common';

@UseGuards(FirebaseAuthGuard)
@Controller('app')
export class AppController {}
```

if only some routes should be protected, add the `@UseGuards(FirebaseAuthGuard)`
decorator to the operation method.

```typescript
import { FirebaseAuthGuard } from '@rxap/nest-firebase';
import { UseGuards } from '@nestjs/common';

@Controller('app')
export class AppController {

  @UseGuards(FirebaseAuthGuard)
  @Get()
  public helloWorld() {}
  
}
```

### Access the firebase user object

If the route is protected by the `FirebaseAuthGuard` and the user is authenticated,
the firebase user object is injected into the request object.

To access the user object, the decorator `@FirebaseUser()` can be used.

```typescript
import { FirebaseUser, DecodedIdToken } from '@rxap/nest-firebase';
import { auth } from 'firebase-admin';
import DecodedIdToken = auth.DecodedIdToken;

@Controller('app')
export class AppController {

  @Get()
  public helloWorld(@FirebaseUser() user: DecodedIdToken) {
    console.log('firebase user id: ', user.uid);
    console.log('firebase user email: ', user.email);
    console.log('is anonymous user: ', res.firebase?.sign_in_provider === 'anonymous');
  }
  
}
```

### Customisation

To add custom logic to the firebase auth token validation create new Nest Guard
and extend the `FirebaseAuthGuard` and declare the method `validateIdToken`.

```typescript
import { FirebaseAuthGuard, FirebaseUser } from '@rxap/nest-firebase';
import { Request } from 'express';

@Injectable()
export class CustomFirebaseAuthGuard extends FirebaseAuthGuard {

  public validateIdToken(idToken: string, request: Request): Promise<FirebaseUser | null> {
    return null;
  }
  
}
```

In the main `AppModule` in the providers section, overwrite the `FirebaseAuthGuard`
with a provider definition.

```typescript
@Module({
  providers: [
    {
      provide: FirebaseAuthGuard,
      useClass: CustomFirebaseAuthGuard
    }
  ]
})
export class AppModule {}
```
