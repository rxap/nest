@rxap/nest
======

[![npm version](https://img.shields.io/npm/v/@rxap/nest?style=flat-square)](https://www.npmjs.com/package/@rxap/nest)
[![commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](https://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
![Libraries.io dependency status for latest release, scoped npm package](https://img.shields.io/librariesio/release/npm/@rxap/nest)
![npm](https://img.shields.io/npm/dm/@rxap/nest)
![NPM](https://img.shields.io/npm/l/@rxap/nest)

> 

- [Installation](#installation)
- [Schematics](#schematics)

# Installation

```
  ng add @rxap/nest
  ```

  *Setup the package @rxap/nest for the workspace.*

# Schematics

    ## init
    > Init the nestjs project.

    ```
    ng g @rxap/nest:init
    ```

    Option | Type | Default | Description
    --- | --- | --- | ---
      project | string |  | The name of the project.
      google | boolean | false | Whether this service will be deployed on google resources
      sentry | boolean | false | Whether this service should use sentry
      swagger | boolean | false | Whether this service should use swagger
      healthIndicator | boolean | false | Whether this service should use a health indicator
      healthIndicatorList | array |  | A list of health indicators
      validator | boolean | true | Whether this service use the ValidationPipe
      platform | string | express | 
      port | number | 3000 | The default port where the server is listens
      apiPrefix | string |  | The default global api prefix
      pluginBuildInfoOptions | object |  | 
      pluginDockerOptions | object |  | 
      sentryDsn | string |  | Default sentry dsn

      | Required |
      | --- |
        | project |

    ## health-indicator
    > Adds a indicator class to the app.

    ```
    ng g @rxap/nest:health-indicator
    ```

    Option | Type | Default | Description
    --- | --- | --- | ---
      name | string |  | The name of the health indicator class
      project | string |  | The name of the project.

      | Required |
      | --- |
        | name |
        | project |

    ## config
    > Adds all files to support the @nestjs/config package.

    ```
    ng g @rxap/nest:config
    ```

    Option | Type | Default | Description
    --- | --- | --- | ---
      project | string |  | The name of the project.

      | Required |
      | --- |
        | project |

    ## ng-add
    > Setup the package @rxap/nest for the workspace.

    ```
    ng g @rxap/nest:ng-add
    ```

    Option | Type | Default | Description
    --- | --- | --- | ---
      init | boolean | false | Whether the init schematic is executed
      project | string |  | The name of the project.


    ## crud
    > Genrate a collection of crud service based on a yaml file to communicated with firestore

    ```
    ng g @rxap/nest:crud
    ```

    Option | Type | Default | Description
    --- | --- | --- | ---
      project | string | crud | 
      overwrite | boolean |  | 


    ## crud-service
    > Genrate crud service to communicated with firestore

    ```
    ng g @rxap/nest:crud-service
    ```

    Option | Type | Default | Description
    --- | --- | --- | ---
      name | string |  | The name of the service.
      path | string |  | The path to create the service.
      language | string |  | Nest service language (ts/js).
      sourceRoot | string |  | Nest service source root directory.
      project | string | crud | 
      flat | boolean | false | Flag to indicate if a directory is created.
      spec | boolean | true | Specifies if a spec file is generated.
      collection2 | array |  | 
      overwrite | boolean |  | 

      | Required |
      | --- |
        | name |

    ## crud-init
    > Init the crud service library

    ```
    ng g @rxap/nest:crud-init
    ```

    Option | Type | Default | Description
    --- | --- | --- | ---
      project | string | crud | 
      importPath | string |  | The library name used to import it, like @myorg/my-awesome-lib. Must be a valid npm name.


    ## sentry
    > Adds Modules, Services and Packages to integrate sentry

    ```
    ng g @rxap/nest:sentry
    ```

    Option | Type | Default | Description
    --- | --- | --- | ---
      project | string |  | The name of the project.
      dsn | string |  | Default sentry dsn

      | Required |
      | --- |
        | project |

    ## validator
    > Adds the validator features to the project

    ```
    ng g @rxap/nest:validator
    ```

    Option | Type | Default | Description
    --- | --- | --- | ---
      project | string |  | The name of the project.

      | Required |
      | --- |
        | project |

    ## swagger
    > Adds the swagger features to the project

    ```
    ng g @rxap/nest:swagger
    ```

    Option | Type | Default | Description
    --- | --- | --- | ---
      project | string |  | The name of the project.

      | Required |
      | --- |
        | project |

