import {
  IsBoolean,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  IsUrl
} from 'class-validator';
import { Type } from 'class-transformer';

/**
 * @deprecated removed use Joi instead
 */
export class SentryConfig {

  @IsUrl()
  dsn!: string;

  @IsOptional()
  @IsBoolean()
  enabled: boolean = false;

  @IsOptional()
  @IsString()
  environment?: string;

  @IsOptional()
  @IsString()
  release?: string;

}

function getPort() {
  let port = 3000;
  if (process.env.PORT) {
    port = Number(process.env.PORT);
    if (isNaN(port)) {
      port = 3000;
    }
  }
  return port;
}

/**
 * @deprecated removed use Joi instead
 */
export class BaseConfig {

  @IsOptional()
  @Type(() => SentryConfig)
  public sentry?: SentryConfig;

  @IsPositive()
  @IsNumber()
  public port: number = getPort();

  @IsOptional()
  @IsString()
  public globalPrefix?: string;

}
