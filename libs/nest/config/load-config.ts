import { BaseConfig } from './base-config';
import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';

/**
 * @deprecated removed use utilities package
 */
export function camelize(str: string): string {
  return str
    .replace(/(-|_|\.|\s)+(.)?/g, (_match: string, _separator: string, chr: string) => {
      return chr ? chr.toUpperCase() : '';
    })
    .replace(/^([A-Z])/, (match: string) => match.toLowerCase());
}

/**
 * @deprecated removed use utilities package
 */
export function SetObjectValue(obj: any, path: string, value: any): void {
  const fragments: string[] = path.split('.').filter(Boolean);

  if (fragments.length === 0) {
    return;
  }

  if (obj && typeof obj === 'object') {
    const fragment: string = fragments.shift()!;

    if (obj.hasOwnProperty(fragment)) {

      if (fragments.length === 0) {
        obj[ fragment ] = value;
      } else {
        SetObjectValue(obj[ fragment ], fragments.join('.'), value);
      }

    } else {

      if (fragments.length === 0) {
        obj[ fragment ] = value;
      } else {
        obj[ fragment ] = {};
        SetObjectValue(obj[ fragment ], fragments.join('.'), value);
      }

    }

  }
}

/**
 * @deprecated removed use Joi instead
 */
export function LoadConfig<T extends BaseConfig>(Config: new (...args: any[]) => T): () => T {
  return () => {
    const config: T = {} as any;

    for (const [ key, value ] of Object.entries(process.env)) {

      if (key.match(/^RXAP_/)) {

        const segments = key.split('_').map(camelize);
        // remove RXAP segment
        segments.shift();

        SetObjectValue(config, segments.join('.'), value);

      }

    }

    const validatedConfig = plainToClass(
      Config,
      config,
      {
        enableImplicitConversion: true,
        exposeDefaultValues: true
      }
    );
    const errors = validateSync(
      validatedConfig,
      {
        skipMissingProperties: false
      });

    if (errors.length > 0) {
      throw new Error(errors.toString());
    }
    return validatedConfig;
  };

}
