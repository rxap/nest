import { BaseConfig } from './base-config';
import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';

/**
 * @deprecated removed use Joi instead
 * @param Config
 * @constructor
 */
export function ValidateConfigFactory<T extends BaseConfig>(Config: new (...args: any[]) => T) {
  return (config: Record<string, any>) => {
    const validatedConfig = plainToClass(
      Config,
      config,
      {
        enableImplicitConversion: true,
        exposeDefaultValues:      true
      }
    );
    const errors          = validateSync(
      validatedConfig,
      {
        skipMissingProperties: false
      });

    if (errors.length > 0) {
      throw new Error(errors.toString());
    }
    return validatedConfig;
  };
}
