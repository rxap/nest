import { Controller, Get } from '@nestjs/common';

@Controller('_ah')
export class WarmupController {

  @Get('warmup')
  public warmup() {
    return true;
  }

}
