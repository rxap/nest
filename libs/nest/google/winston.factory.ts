import * as winston from 'winston';
import {
  utilities as nestWinstonModuleUtilities,
  WinstonModuleOptions
} from 'nest-winston';

export function WinstonFactory(level = 'info'): WinstonModuleOptions {

  if (process.env.GAE_APPLICATION) {

    console.log('use google cloud winston logger');

    // Imports the Google Cloud client library for Winston
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const { LoggingWinston } = require('@google-cloud/logging-winston');

    const loggingWinston = new LoggingWinston();

    return {
      level,
      transports: [
        // Add Stackdriver Logging
        loggingWinston
      ]
    };

  }

  return {
    level,
    transports: [
      new winston.transports.Console({
        format: nestWinstonModuleUtilities.format.nestLike()
      })
    ]
  };

}
