import { ArgumentsHost, Catch, HttpException, HttpServer, HttpStatus, Inject, Logger } from '@nestjs/common';
import { Request } from 'express';
import { BaseExceptionFilter } from '@nestjs/core';

@Catch(HttpException)
export class HttpExceptionFilter extends BaseExceptionFilter {

  @Inject(Logger)
  private readonly logger?: Logger;

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();

    this.logger?.debug(exception.message, `(${status}) ${request.method.toUpperCase()} ${request.path}`);

    super.catch(exception, host);
  }
}
