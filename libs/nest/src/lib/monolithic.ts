import { Server } from './server';
import { INestApplication, Logger, NestApplicationOptions } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { DetermineVersion } from './determine-version';

export interface MonolithicBootstrapOptions {
  publicUrl: string;
  port: number;
  version: string;
}

export class Monolithic<O extends NestApplicationOptions, T extends INestApplication = INestApplication> extends Server<O, T, MonolithicBootstrapOptions> {

  protected override create(): Promise<T> {
    return NestFactory.create<T>(this.module, this.options);
  }

  protected override prepareOptions(app: T): MonolithicBootstrapOptions {

    const logger                            = app.get(Logger);
    const config: ConfigService<unknown> = app.get(ConfigService);

    logger.log('environment: ' + JSON.stringify(this.environment, undefined, this.environment.production ? undefined : 2), 'Bootstrap');

    const globalPrefix = config.get('GLOBAL_API_PREFIX') ?? config.get('globalPrefix') ?? '';

    if (globalPrefix) {
      app.setGlobalPrefix(globalPrefix);
    }

    logger.debug('Server Config: ' + JSON.stringify((config as any).internalConfig, undefined, this.environment.production ? undefined : 2), 'Bootstrap');

    const port = config.get('PORT') ?? config.get('port') ?? 3333;

    return {
      publicUrl: (config.get('PUBLIC_URL') ?? 'http://localhost:' + port) + (globalPrefix ? '/' + globalPrefix + '/' : '/'),
      port,
      version: DetermineVersion(this.environment),
    };
  }

  protected override listen(app: T, logger: Logger, options: MonolithicBootstrapOptions): Promise<any> {
    return app.listen(options.port, () => {
      logger.log('Listening at ' + options.publicUrl, 'Bootstrap')
    })
  }

}
