import { CrudInitSchema } from './schema';
import {
  Rule,
  externalSchematic,
  chain
} from '@angular-devkit/schematics';
import { strings } from '@angular-devkit/core';

const { dasherize } = strings;

export default function(options: CrudInitSchema): Rule {

  return chain([
    externalSchematic('@nrwl/nest', 'library', { name: options.project, importPath: options.importPath }),
    tree => tree.create(`libs/${dasherize(options.project)}/src/db.yaml`, 'collections: []')
  ]);

}
