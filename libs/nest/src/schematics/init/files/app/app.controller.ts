import { Controller, Get } from '@nestjs/common';
import { environment } from '../environments/environment';

@Controller()
export class AppController {
  @Get()
  environment() {
    return environment;
  }

}
