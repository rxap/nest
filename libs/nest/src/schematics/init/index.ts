import {
  apply,
  applyTemplates,
  chain, externalSchematic,
  forEach,
  mergeWith,
  move, noop,
  Rule,
  schematic, SchematicsException,
  Tree,
  url
} from '@angular-devkit/schematics';
import { InitSchema } from './schema';
import {
  AddPackageJsonDependency,
  AddPackageJsonDevDependency, GetPackageJson, GetProjectRoot,
  GetProjectSourceRoot,
  InstallNodePackages
} from '@rxap/schematics-utilities';
import { formatFiles, updateWorkspace } from '@nrwl/workspace';
import { IndentationText, Project, QuoteKind, WriterFunctionOrValue, Writers } from 'ts-morph';
import {
  AddDir,
  AddNestModuleController,
  AddNestModuleImport,
  AddNestModuleProvider,
  ApplyTsMorphProject
} from '@rxap/schematics-ts-morph';
import { join } from 'path';

function UpdateAppModule(options: InitSchema): Rule {

  return (tree: Tree) => {

    const projectSourceRoot = GetProjectSourceRoot(tree, options.project);

    const project = new Project({
      manipulationSettings: {
        indentationText: IndentationText.TwoSpaces,
        quoteKind: QuoteKind.Single
      },
      useInMemoryFileSystem: true
    });

    AddDir(tree.getDir(projectSourceRoot), project);

    const sourceFile = project.getSourceFile('/app/app.module.ts');

    if (!sourceFile) {
      throw new Error('Could not find the app module');
    }

    AddNestModuleImport(
      sourceFile,
      'ThrottlerModule',
      [
        {
          moduleSpecifier: '@nestjs/throttler',
          namedImports: [ 'ThrottlerModule' ]
        }
      ],
      w => {
        w.writeLine('ThrottlerModule.forRoot(');
        Writers.object({
          ttl: '1',
          limit: '10'
        })(w);
        w.write(')');
      }
    );

    AddNestModuleImport(
      sourceFile,
      'ConfigModule',
      [
        {
          moduleSpecifier: '@nestjs/config',
          namedImports: [ 'ConfigModule' ]
        },
        {
          moduleSpecifier: 'joi',
          namespaceImport: 'Joi'
        },
        ...(options.sentry ? [
          {
            moduleSpecifier: '@rxap/nest',
            namedImports: [ 'DetermineRelease', 'DetermineEnvironment' ]
          }
        ] : []),
      ],
      w => {
        w.writeLine('ConfigModule.forRoot(');
        Writers.object({
          isGlobal: 'true',
          validationSchema: w1 => {
            w1.write(' Joi.object(');
            const obj: Record<string, WriterFunctionOrValue | undefined> = {
              PORT: `Joi.number().default(${options.port})`,
              GLOBAL_API_PREFIX: `Joi.string()${options.apiPrefix ? `.default('${options.apiPrefix}')` : ''}`
            };
            if (options.sentry) {
              obj.SENTRY_DSN = `Joi.string()${options.sentryDsn ? `.default('${options.sentryDsn}')` : ''}`;
              obj.SENTRY_ENABLED = 'Joi.string().default(environment.sentry?.enabled ?? false)';
              obj.SENTRY_ENVIRONMENT = 'DetermineEnvironment(environment) ? Joi.string().default(DetermineEnvironment(environment)) : Joi.string()';
              obj.SENTRY_RELEASE = 'DetermineRelease(environment) ? Joi.string().default(DetermineRelease(environment)) : Joi.string()';
              obj.SENTRY_SERVER_NAME = `Joi.string().default('${options.project}')`;
              obj.SENTRY_DEBUG = 'Joi.string().default(environment.sentry?.debug ?? false)';
            }
            Writers.object(obj)(w1);
            w1.write(')');
          }
        })(w);
        w.write(')');
      }
    );

    AddNestModuleProvider(
      sourceFile,
      {
        provide: 'APP_GUARD',
        useClass: 'ThrottlerGuard'
      },
      [
        {
          namedImports: [ 'APP_GUARD' ],
          moduleSpecifier: '@nestjs/core'
        },
        {
          namedImports: [ 'ThrottlerGuard' ],
          moduleSpecifier: '@nestjs/throttler'
        }
      ]
    );

    AddNestModuleProvider(
      sourceFile,
      {
        provide: 'ENVIRONMENT',
        useValue: 'environment'
      },
      [
        {
          namedImports: [ 'ENVIRONMENT' ],
          moduleSpecifier: '@rxap/nest'
        },
        {
          namedImports: [ 'environment' ],
          moduleSpecifier: '../environments/environment'
        }
      ]
    );

    AddNestModuleProvider(
      sourceFile,
      'Logger',
      [
        {
          namedImports: [ 'Logger' ],
          moduleSpecifier: '@nestjs/common'
        }
      ]
    );

    if (options.google) {
      AddNestModuleController(
        sourceFile,
        'WarmupController',
        [
          {
            namedImports: [ 'WarmupController' ],
            moduleSpecifier: '@rxap/nest'
          }
        ]
      );
    }

    return ApplyTsMorphProject(project, projectSourceRoot);
  }

}

export default function(options: InitSchema): Rule {

  return async (host: Tree) => {

    const projectSourceRoot = GetProjectSourceRoot(host, options.project);

    const addPackageRules: Rule[] = [];

    if (options.google) {
      addPackageRules.push(
        AddPackageJsonDependency('@google-cloud/logging-winston', 'latest', { soft: true }),
        AddPackageJsonDependency('winston', 'latest', { soft: true }),
        AddPackageJsonDependency('nest-winston', 'latest', { soft: true }),
      );
    }

    switch (options.platform) {
      case 'express':
        addPackageRules.push(
          AddPackageJsonDependency('@nestjs/platform-express', 'latest', { soft: true }),
          AddPackageJsonDependency('helmet', 'latest', { soft: true }),
        );
        break;
      case 'fastify':
        addPackageRules.push(
          AddPackageJsonDependency('@nestjs/platform-fastify', 'latest', { soft: true }),
          AddPackageJsonDependency('@fastify/helmet', 'latest', { soft: true }),
        );
        break;
      default:
        throw new SchematicsException('The underlying platform is not specified')
    }

    if (options.healthIndicatorList?.length) {
      options.healthIndicator = true;
    }

    options.pluginBuildInfoOptions ??= {};
    options.pluginDockerOptions ??= {};

    options.pluginBuildInfoOptions.project = options.project;
    options.pluginDockerOptions.project = options.project;

    return chain([
      tree => {
        tree.delete(join(projectSourceRoot, 'app', 'app.controller.ts'));
        tree.delete(join(projectSourceRoot, 'app', 'app.controller.spec.ts'));
        tree.delete(join(projectSourceRoot, 'app', 'app.service.spec.ts'));
        tree.delete(join(projectSourceRoot, 'app', 'app.service.ts'));
        tree.delete(join(projectSourceRoot, 'app', 'app.module.ts'));
      },
      mergeWith(apply(url('./files'), [
        applyTemplates(options),
        move(projectSourceRoot),
        forEach(entry => {
          if (host.exists(entry.path)) {
            host.overwrite(entry.path, entry.content);
            return null;
          }
          return entry;
        })
      ])),
      UpdateAppModule(options),
      tree => {
        if (tree.exists(join(projectSourceRoot, 'app', 'app.controller.spec.ts'))) {
          tree.delete(join(projectSourceRoot, 'app', 'app.controller.spec.ts'));
        }
        if (tree.exists(join(projectSourceRoot, 'app', 'app.service.spec.ts'))) {
          tree.delete(join(projectSourceRoot, 'app', 'app.service.spec.ts'));
        }
        if (tree.exists(join(projectSourceRoot, 'app', 'app.service.ts'))) {
          tree.delete(join(projectSourceRoot, 'app', 'app.service.ts'));
        }
      },
      updateWorkspace(workspace => {
        const project = workspace.projects.get(options.project);
        if (project) {
          const build = project.targets.get('build');
          if (build) {
            build.options ??= {};
            build.options['generatePackageJson'] = true;
            build.options.assets ??= [];
            const assets = build.options.assets as string[];
            const dockerfilePath = join(projectSourceRoot, 'Dockerfile');
            if (!assets.includes(dockerfilePath)) {
              assets.push(dockerfilePath);
            }
            const healthcheckPath = join(projectSourceRoot, 'healthcheck.js');
            if (!assets.includes(healthcheckPath)) {
              assets.push(healthcheckPath);
            }
          }
          if (!project.targets.has('deploy')) {
            project.targets.set('deploy', {
              builder: '@nrwl/workspace:run-commands',
              options: {
                commands: [ 'echo ok' ]
              },
              configurations: {
                production: {}
              }
            })
          }
        }
      }),
      ...addPackageRules,
      AddPackageJsonDependency('@nestjs/throttler', 'latest', { soft: true }),
      AddPackageJsonDependency('cookie-parser', 'latest', { soft: true }),
      AddPackageJsonDependency('@nestjs/config', 'latest', { soft: true }),
      AddPackageJsonDependency('joi', 'latest', { soft: true }),
      AddPackageJsonDevDependency('@types/cookie-parser', 'latest', { soft: true }),
      AddPackageJsonDevDependency('@types/csurf', 'latest', { soft: true }),
      AddPackageJsonDevDependency('@rxap/plugin-build-info', 'latest', { soft: true }),
      AddPackageJsonDevDependency('@rxap/plugin-docker', 'latest', { soft: true }),
      tree => {
        const rootPackageJson = GetPackageJson(tree);
        const packageJsonFilePath = join(projectSourceRoot, '..', 'package.json');
        if (!tree.exists(packageJsonFilePath)) {
          tree.create(packageJsonFilePath, '{}');
        }
        const content: any = JSON.parse(tree.read(packageJsonFilePath)?.toString('utf-8') ?? '{}');
        content.dependencies ??= {};
        content.dependencies.joi ??= rootPackageJson.dependencies?.joi ?? 'latest';
        tree.overwrite(packageJsonFilePath, JSON.stringify(content, undefined, 2));
      },
      InstallNodePackages(),
      externalSchematic('@rxap/plugin-build-info', 'config', options.pluginBuildInfoOptions),
      externalSchematic('@rxap/plugin-docker', 'config', options.pluginDockerOptions),
      options.healthIndicator ? () => {
        if (options.healthIndicatorList?.length) {
          return chain(options.healthIndicatorList.map(name => schematic('health-indicator', { project: options.project, name })));
        } else {
          return schematic('health-indicator', { project: options.project });
        }
      } : noop(),
      options.sentry ? schematic('sentry', { project: options.project }) : noop(),
      options.validator ? schematic('validator', { project: options.project }) : noop(),
      options.swagger ? schematic('swagger', { project: options.project }) : noop(),
      formatFiles()
    ]);

  };

}
