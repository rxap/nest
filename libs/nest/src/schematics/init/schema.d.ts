export interface InitSchema {
  project: string;
  google: boolean;
  sentry: boolean;
  swagger: boolean;
  healthIndicator: boolean;
  platform: 'express' | 'fastify';
  validator: boolean;
  healthIndicatorList?: string[];
  pluginBuildInfoOptions?: Record<string, unknown>;
  pluginDockerOptions?: Record<string, unknown>;
  port: number;
  apiPrefix?: string;
  sentryDsn?: string;
}
