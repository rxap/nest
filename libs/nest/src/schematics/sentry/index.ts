import { SentrySchema } from './schema';
import { chain, Rule, Tree } from '@angular-devkit/schematics';
import {
  AddPackageJsonDependency,
  GetPackageJson,
  GetProjectSourceRoot,
  InstallNodePackages
} from '@rxap/schematics-utilities';
import { IndentationText, Project, QuoteKind, Writers } from 'ts-morph';
import { AddDir, AddNestModuleImport, AddNestModuleProvider, ApplyTsMorphProject } from '@rxap/schematics-ts-morph';
import { join } from 'path';

export default function(options: SentrySchema): Rule {

  return (host: Tree) => {

    const projectSourceRoot = GetProjectSourceRoot(host, options.project);

    const project = new Project({
      manipulationSettings: {
        indentationText: IndentationText.TwoSpaces,
        quoteKind: QuoteKind.Single
      },
      useInMemoryFileSystem: true
    });

    AddDir(host.getDir(projectSourceRoot), project);

    const sourceFile = project.getSourceFile('/app/app.module.ts');

    if (!sourceFile) {
      throw new Error('Could not find the app module');
    }

    AddNestModuleImport(
      sourceFile,
      'SentryModule',
      [
        {
          moduleSpecifier: '@rxap/nest-sentry',
          namedImports: [ 'SentryModule' ]
        },
        {
          moduleSpecifier: '@nestjs/config',
          namedImports: [ 'ConfigService', 'ConfigModule' ]
        },
        {
          namedImports: [ 'environment' ],
          moduleSpecifier: '../environments/environment'
        }
      ],
      w => {
        w.writeLine('SentryModule.forRootAsync(');
        Writers.object({
          imports: '[ ConfigModule ]',
          inject: '[ ConfigService ]',
          useFactory: w1 => {
            w1.write('async (config: ConfigService) => (');
            Writers.object({
              dsn: `config.getOrThrow('SENTRY_DSN')`,
              enabled: `config.get('SENTRY_ENABLED', false)`,
              environment: `config.get('SENTRY_ENVIRONMENT')`,
              release: `config.get('SENTRY_RELEASE')`,
              serverName: `config.get('SENTRY_SERVER_NAME')`,
              debug: `config.get('SENTRY_DEBUG', false)`,
              tracesSampleRate: `1.0`,
              logLevels: `[ 'error', 'warn' ]`,
              maxValueLength: 'Number.MAX_SAFE_INTEGER',
            })(w1);
            w1.write(')')
          }
        })(w);
        w.write(')');
      }
    );
    AddNestModuleProvider(
      sourceFile,
      {
        provide: 'SENTRY_INTERCEPTOR_OPTIONS',
        useValue: Writers.object({
          filters: w1 => {
            w1.write('[');
            Writers.object({
              type: 'HttpException',
              filter: '(exception: HttpException) => 500 > exception.getStatus()'
            })(w1);
            w1.write(']');
          }
        })
      },
      [
        {
          namedImports: [ 'SENTRY_INTERCEPTOR_OPTIONS' ],
          moduleSpecifier: '@rxap/nest-sentry'
        },
        {
          namedImports: [ 'HttpException' ],
          moduleSpecifier: '@nestjs/common'
        }
      ]
    );
    AddNestModuleProvider(
      sourceFile,
      {
        provide: 'APP_INTERCEPTOR',
        useClass: 'SentryInterceptor',
      },
      [
        {
          namedImports: [ 'APP_INTERCEPTOR' ],
          moduleSpecifier: '@nestjs/core'
        },
        {
          namedImports: [ 'SentryInterceptor' ],
          moduleSpecifier: '@rxap/nest-sentry'
        },
      ]
    );

    return chain([
      ApplyTsMorphProject(project, projectSourceRoot),
      AddPackageJsonDependency('@sentry/node', 'latest', { soft: true }),
      AddPackageJsonDependency('@sentry/hub', 'latest', { soft: true }),
      AddPackageJsonDependency('@rxap/nest-sentry', 'latest', { soft: true }),
      tree => {
        const rootPackageJson = GetPackageJson(tree);
        const packageJsonFilePath = join(projectSourceRoot, '..', 'package.json');
        if (!tree.exists(packageJsonFilePath)) {
          tree.create(packageJsonFilePath, '{}');
        }
        const content: any = JSON.parse(tree.read(packageJsonFilePath)?.toString('utf-8') ?? '{}');
        content.dependencies ??= {};
        content.dependencies['@sentry/hub'] ??= rootPackageJson.dependencies!['@sentry/hub'] ?? 'latest';
        tree.overwrite(packageJsonFilePath, JSON.stringify(content, undefined, 2));
      },
      InstallNodePackages(),
    ])

  }

}
