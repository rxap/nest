import { SwaggerSchema } from './schema';
import {
  apply,
  applyTemplates,
  chain,
  forEach,
  mergeWith,
  move,
  Rule,
  SchematicsException,
  Tree,
  url
} from '@angular-devkit/schematics';
import { AddPackageJsonDependency, GetProjectSourceRoot, InstallNodePackages } from '@rxap/schematics-utilities';
import { updateWorkspace } from '@nrwl/workspace';

export default function(options: SwaggerSchema): Rule {

  return async (host: Tree) => {

    const projectSourceRoot = GetProjectSourceRoot(host, options.project);

    return chain([
      mergeWith(apply(url('./files'), [
        applyTemplates(options),
        move(projectSourceRoot),
        forEach(entry => {
          if (host.exists(entry.path)) {
            host.overwrite(entry.path, entry.content);
            return null;
          }
          return entry;
        })
      ])),
      updateWorkspace(workspace => {
        const project = workspace.projects.get(options.project);
        if (project) {
          const build = project.targets.get('build');
          if (!build) {
            throw new SchematicsException('The selected project does not have a build target')
          }
          if (!build.options) {
            throw new SchematicsException('The selected project has the build target without options')
          }
          if (!build.options.outputPath) {
            throw new SchematicsException('The selected project has the build target without the option outputPath')
          }
          if (!build.options.tsConfig) {
            throw new SchematicsException('The selected project has the build target without the option tsConfig')
          }
          project.targets.set('swagger-build', {
            builder: "@nrwl/node:webpack",
            options: {
              outputPath: (build.options.outputPath as string).replace('dist/apps/', 'dist/swagger/'),
              main: `${project.sourceRoot}/swagger.ts`,
              transformers: ["@nestjs/swagger/plugin"],
              tsConfig: build.options.tsConfig,
              fileReplacements: [
                {
                  replace: `${project.sourceRoot}/environments/environment.ts`,
                  with: `${project.sourceRoot}/environments/environment.swagger.ts`
                }
              ]
            }
          })
        }
      }),
      AddPackageJsonDependency('swagger-ui-express', 'latest', { soft: true }),
      AddPackageJsonDependency('@nestjs/swagger', 'latest', { soft: true }),
      InstallNodePackages(),
    ])

  }

}
