import { SentrySchema } from '../sentry/schema';
import { chain, Rule, Tree } from '@angular-devkit/schematics';
import { AddPackageJsonDependency, GetProjectSourceRoot, InstallNodePackages } from '@rxap/schematics-utilities';
import { IndentationText, Project, QuoteKind } from 'ts-morph';
import { AddDir, ApplyTsMorphProject } from '@rxap/schematics-ts-morph';

export default function(options: SentrySchema): Rule {

  return (host: Tree) => {

    const projectSourceRoot = GetProjectSourceRoot(host, options.project);

    const project = new Project({
      manipulationSettings: {
        indentationText: IndentationText.TwoSpaces,
        quoteKind: QuoteKind.Single
      },
      useInMemoryFileSystem: true
    });

    AddDir(host.getDir(projectSourceRoot), project);

    const sourceFile = project.getSourceFile('/app/app.module.ts');

    if (!sourceFile) {
      throw new Error('Could not find the app module');
    }

    return chain([
      ApplyTsMorphProject(project, projectSourceRoot),
      AddPackageJsonDependency('class-validator', 'latest', { soft: true }),
      AddPackageJsonDependency('class-transformer', 'latest', { soft: true }),
      InstallNodePackages(),
    ]);

  };
}
