# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 9.0.2 (2022-12-28)

**Note:** Version bump only for package @rxap/nest-sentry





## 9.0.1 (2022-12-28)

**Note:** Version bump only for package @rxap/sentry
