import { ConsoleLogger, Inject, Injectable } from '@nestjs/common';
import * as Sentry from '@sentry/node';
import { SentryModuleOptions } from './sentry.interfaces';
import { SENTRY_MODULE_OPTIONS } from './tokens';

@Injectable()
export class SentryLogger extends ConsoleLogger {

  constructor(
    @Inject(SENTRY_MODULE_OPTIONS)
  readonly opts?: SentryModuleOptions,
    ) {
    super();
  }

  log(message: string, context?: string, asBreadcrumb?: boolean) {
    super.log(message, context);
    if (this.opts?.logLevels && !this.opts.logLevels.includes('log')) {
      return;
    }
    try {
      asBreadcrumb ?
      Sentry.addBreadcrumb({
        message,
        level: 'log',
        data: {
          context
        }
      }) :
      Sentry.captureMessage(message, {
        level: 'log',
      });
    } catch (err) {}
  }

  error(message: string, stack?: string, context?: string) {
    super.error(message, stack, context);
    if (this.opts?.logLevels && !this.opts.logLevels.includes('error')) {
      return;
    }
    try {
      Sentry.captureMessage(message, { level: 'error' });
    } catch (err) {}
  }

  warn(message: string, context?: string, asBreadcrumb?: boolean) {
    super.warn(message, context);
    if (this.opts?.logLevels && !this.opts.logLevels.includes('warn')) {
      return;
    }
    try {
      asBreadcrumb ?
      Sentry.addBreadcrumb({
        message,
        level: 'warning',
        data: {
          context
        }
      }) :
      Sentry.captureMessage(message, { level: 'warning' });
    } catch (err) {}
  }

  debug(message: string, context?: string, asBreadcrumb?: boolean) {
    super.debug(message, context);
    if (this.opts?.logLevels && !this.opts.logLevels.includes('debug')) {
      return;
    }
    try {
      asBreadcrumb ?
      Sentry.addBreadcrumb({
        message,
        level: 'debug',
        data: {
          context
        }
      }) :
      Sentry.captureMessage(message, { level: 'debug' });
    } catch (err) {}
  }

}
