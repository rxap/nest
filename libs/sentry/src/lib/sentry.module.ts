import { DynamicModule, Module, Provider, Type } from '@nestjs/common';
import { SENTRY_MODULE_OPTIONS } from './tokens';
import { SentryModuleAsyncOptions, SentryModuleOptions, SentryOptionsFactory } from './sentry.interfaces';
import { SentryLogger } from './sentry.logger';
import { SentryService } from './sentry.service';

@Module({
  providers: [ SentryLogger, SentryService ],
  exports: [ SentryLogger, SentryService ]
})
export class SentryModule {
  public static forRoot(options: SentryModuleOptions): DynamicModule {
    return {
      exports: [SentryService],
      module: SentryModule,
      providers: [
        {
          provide: SENTRY_MODULE_OPTIONS,
          useValue: options,
        },
      ],
    };
  }

  public static forRootAsync(
    options: SentryModuleAsyncOptions,
  ): DynamicModule {
    return {
      exports: [SentryService],
      imports: options.imports,
      module: SentryModule,
      providers: [
        ...this.createAsyncProviders(options),
      ],
    };
  }

  private static createAsyncProviders(
    options: SentryModuleAsyncOptions,
  ): Provider[] {
    if (options.useExisting || options.useFactory) {
      return [this.createAsyncOptionsProvider(options)];
    }
    const useClass = options.useClass as Type<SentryOptionsFactory>;
    return [
      this.createAsyncOptionsProvider(options),
      {
        provide: useClass,
        useClass,
      },
    ];
  }

  private static createAsyncOptionsProvider(
    options: SentryModuleAsyncOptions,
  ): Provider {
    if (options.useFactory) {
      return {
        inject: options.inject || [],
        provide: SENTRY_MODULE_OPTIONS,
        useFactory: options.useFactory,
      };
    }
    const inject = [
      (options.useClass || options.useExisting) as Type<SentryOptionsFactory>,
    ];
    return {
      provide: SENTRY_MODULE_OPTIONS,
      useFactory: async (optionsFactory: SentryOptionsFactory) =>
        await optionsFactory.createSentryModuleOptions(),
      inject,
    };
  }

}
