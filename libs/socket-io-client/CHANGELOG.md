# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [9.0.1](https://gitlab.com/rxap/nest/compare/@rxap/nest-socket-io-client@9.0.1-dev.0...@rxap/nest-socket-io-client@9.0.1) (2023-01-23)

**Note:** Version bump only for package @rxap/nest-socket-io-client





## 9.0.1-dev.0 (2023-01-09)

**Note:** Version bump only for package @rxap/nest-socket-io-client
